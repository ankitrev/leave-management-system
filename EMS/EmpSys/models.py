from django.db import models
# Create your models here.
class Employee(models.Model):
    empid = models.IntegerField(primary_key = True)
    name = models.CharField(max_length = 50)
    dob = models.DateField()
    dept = models.ForeignKey('Department')
    designation = models.ForeignKey('Designation')
    doj = models.DateField()
    last_working_day = models.DateField()
    def __unicode__(self):
        return self.name

class Department(models.Model):
    dept_name = models.CharField(max_length = 50)
    hod = models.CharField(max_length = 50)
    intended_strength = models.IntegerField(default = 0)
    def __unicode__(self):
        return self.dept_name

class Designation(models.Model):
    designation_name = models.CharField(max_length = 50)
    def __unicode__(self):
        return self.designation_name

class Leave(models.Model):
    lv_emp_id = models.IntegerField(primary_key = True)
    department = models.ForeignKey('Department')
    lv_status = models.BooleanField(default = False)
    lv_date = models.DateField()
    hod_status = models.BooleanField(default = False)
    def __unicode__(self):
        return str(self.lv_emp_id)
