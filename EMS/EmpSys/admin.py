from django.contrib import admin

from .models import Employee, Department, Designation, Leave

admin.site.register(Employee)
admin.site.register(Department)
admin.site.register(Designation)
admin.site.register(Leave)


# Register your models here.
