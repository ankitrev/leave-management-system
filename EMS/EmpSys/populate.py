from EmpSys.models import Employee, Department, Designation, Leave
from datetime import datetime

d1 = Department(dept_name = 'Technical',hod = 'Sanjeev', intended_strength = 20)
d1.save()

d2 = Department(dept_name = 'Marketing',hod = 'Rajeev', intended_strength = 20)
d2.save()

d3 = Department(dept_name = 'Account',hod = 'Abhay', intended_strength = 30)
d3.save()

de1 = Designation(designation_name = 'Software Engineer')
de1.save()

de2 = Designation(designation_name = 'Sales Manager')
de2.save()

de3 = Designation(designation_name = 'Account Manager')
de3.save()
de4 = Designation(designation_name = 'Chief Technical Officer')
de4.save()

de5 = Designation(designation_name = 'HOD')
de5.save()

Employee(empid = '1', name = 'Sanjeev', dob = '1992-07-13', dept = d1, designation = de5, doj = '2016-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '2', name = 'Rajeev', dob = '1992-07-10', dept = d2, designation = de5, doj = '2015-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '3', name = 'Abhay', dob = '1991-07-13', dept = d3, designation = de5, doj = '2016-08-13',last_working_day = datetime.now().date()).save()
Employee(empid = '4', name = 'Ankit', dob = '1994-07-13', dept = d1, designation = de4, doj = '2010-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '5', name = 'Rakesh', dob = '1998-07-18', dept = d2, designation = de2, doj = '2011-08-15',last_working_day = datetime.now().date()).save()
Employee(empid = '6', name = 'Sachin', dob = '1999-07-15', dept = d3, designation = de3, doj = '2012-09-11',last_working_day = datetime.now().date()).save()
Employee(empid = '7', name = 'Anil', dob = '1990-09-12', dept = d1, designation = de1, doj = '2015-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '8', name = 'Aditya', dob = '1997-09-13', dept = d1, designation = de1, doj = '2009-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '9', name = 'Deepak', dob = '1991-10-11', dept = d1, designation = de1, doj = '2013-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '10', name = 'xyz', dob = '1996-07-13', dept = d2, designation = de2, doj = '2014-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '11', name = 'efg', dob = '1995-07-13', dept = d1, designation = de1, doj = '2011-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '12', name = 'ghg', dob = '1992-06-13', dept = d2, designation = de2, doj = '2001-05-12',last_working_day = datetime.now().date()).save()
Employee(empid = '13', name = 'jjk', dob = '1990-09-10', dept = d2, designation = de2, doj = '2007-07-19',last_working_day = datetime.now().date()).save()
Employee(empid = '14', name = 'sjh', dob = '1999-09-15', dept = d1, designation = de1, doj = '2017-07-19',last_working_day = datetime.now().date()).save()
Employee(empid = '15', name = 'gkg', dob = '1998-09-15', dept = d2, designation = de2, doj = '2013-07-19',last_working_day = datetime.now().date()).save()
Employee(empid = '16', name = 'dgs', dob = '1980-09-15', dept = d3, designation = de3, doj = '2011-09-19',last_working_day = datetime.now().date()).save()
Employee(empid = '17', name = 'dsh', dob = '1982-09-12', dept = d3, designation = de3, doj = '2014-04-14',last_working_day = datetime.now().date()).save()

Leave(lv_emp_id = '1', department = d1, lv_status = True, lv_date = datetime.now().date(), hod_status = True).save()
Leave(lv_emp_id = '2', department = d2, lv_status = True, lv_date = datetime.now().date(), hod_status = True).save()
Leave(lv_emp_id = '4', department = d1, lv_status = True, lv_date = '2018-07-16', hod_status = False).save()
Leave(lv_emp_id = '5', department = d1, lv_status = True, lv_date = '2018-05-11', hod_status = False).save()
Leave(lv_emp_id = '6', department = d2, lv_status = True, lv_date = '2018-08-14', hod_status = False).save()
Leave(lv_emp_id = '8', department = d3, lv_status = True, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '9', department = d1, lv_status = True, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '10', department = d1, lv_status = True, lv_date = '2018-06-18', hod_status = False).save()
Leave(lv_emp_id = '11', department = d1, lv_status = False, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '12', department = d2, lv_status = True, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '13', department = d2, lv_status = True, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '14', department = d1, lv_status = False, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '15', department = d2, lv_status = False, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '16', department = d3, lv_status = True, lv_date = datetime.now().date(), hod_status = False).save()
Leave(lv_emp_id = '17', department = d3, lv_status = False, lv_date = datetime.now().date(), hod_status = False).save()
