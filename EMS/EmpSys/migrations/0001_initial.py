# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dept_name', models.CharField(max_length=50)),
                ('hod', models.CharField(max_length=50)),
                ('intended_strength', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Designation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('designation_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('empid', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('dob', models.DateField()),
                ('doj', models.DateField()),
                ('last_working_day', models.DateField()),
                ('dept', models.ForeignKey(to='EmpSys.Department')),
                ('designation', models.ForeignKey(to='EmpSys.Designation')),
            ],
        ),
        migrations.CreateModel(
            name='Leave',
            fields=[
                ('lv_emp_id', models.IntegerField(serialize=False, primary_key=True)),
                ('lv_status', models.BooleanField(default=False)),
                ('lv_date', models.DateField()),
                ('hod_status', models.BooleanField(default=False)),
                ('department', models.ForeignKey(to='EmpSys.Department')),
            ],
        ),
    ]
