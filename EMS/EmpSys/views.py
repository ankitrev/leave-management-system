from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.views.generic import View
from .models import Employee, Department, Designation, Leave
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils.datastructures import MultiValueDictKeyError
from django.db import IntegrityError
from datetime import datetime

# Method to retrieve the number of pending leave request which are need to be approved by HOD

class LeaveDetails(View):
    def get(self,request):
        try:
            id = request.GET['id']
            res=Department.objects.get(hod = id)
            name=res.dept_name
            no_of_leaves_pending= Leave.objects.filter(department__dept_name= name, lv_status=False).values()
            length = len(no_of_leaves_pending)
            res=[]
        
            for i in range(0,length):
                k = no_of_leaves_pending[i]['lv_emp_id']
                dict_res = {}
                dict_res['emp_id'] = k
                res.append(dict_res)

        #length = len(no_of_leaves_pending)
            return HttpResponse(res,status=200)
       except Department.DoesNotExist:
            print("Invalid Department ID")
        except Leave.DoesNotExist:
            print ("Invalid Input")
            
# Method to retrieve the number of employees on leave today

class LeaveInfo(View):
    def get(self,request):
        try:
            result=[]
            date = datetime.now().date()
            res = Leave.objects.filter(lv_date = date, lv_status = 'True')
            length = len(res)
            return HttpResponse(length, status=200)
        except Exception as exception:
            str(exception)
            return HttpResponse('Internal Server Error', status=500)

# Method to retrieve name of employees who are on leave, for every day in a month

class Leaves(View):

    def get(self,request):
        try:
            id = request.GET['id']
            if id == 'january' or id == 'January':
                res = Leave.objects.filter(lv_date__contains='2018-01').values()
            elif id == 'february' or id == 'February':
                res = Leave.objects.filter(lv_date__contains='2018-02').values()
            elif id == 'march' or id == 'March':
                res = Leave.objects.filter(lv_date__contains='2018-03').values()
            elif id == 'april' or id == 'April':
                res = Leave.objects.filter(lv_date__contains='2018-04').values()
            elif id == 'may' or id == 'May':
                res = Leave.objects.filter(lv_date__contains='2018-05').values()
            elif id == 'june' or id == 'June':
                res = Leave.objects.filter(lv_date__contains='2018-06').values()
            elif id == 'july' or id == 'July':
                res = Leave.objects.filter(lv_date__contains='2018-07').values()
            elif id == 'august' or id == 'August':
                res = Leave.objects.filter(lv_date__contains='2018-08').values()
            elif id == 'september' or id == 'September':
                res = Leave.objects.filter(lv_date__contains='2018-09').values()
            elif id == 'october' or id == 'October':
                res = Leave.objects.filter(lv_date__contains='2018-10').values()
            elif id == 'november' or id == 'November':
                res = Leave.objects.filter(lv_date__contains='2018-11').values()
            elif id == 'december' or id == 'December':
                res = Leave.objects.filter(lv_date__contains='2018-02').values()
            else:
                return HttpResponse('Invalid Input!')

            max=len(res)
            for i in range(0,max):
                k=res[i]['lv_status']
                if k == False:
                    max=max-1

            list1 = []
            list2 = []

            for i in range(0,max):
                k=res[i]['lv_emp_id']
                list1.append(k)

            for i in range(0,max):
                m=res[i]['lv_date']
                list2.append(m)

            j=0
            cmntlist=[]
            for i in list1:
               r=Employee.objects.get(pk=i)
                dict_res = {}
                dict_res['emp_name'] = r.emp_name
                dict_res['date_of_leave'] = list2[j]
                j=j+1
                cmntlist.append(dict_res)
            return HttpResponse(cmntlist, status=200)
        except ObjectDoesNotExist:
            print("Entry doesn't exist.")



class EmployeeData(View):

# Adding leave request
    def post(self,request):
        try:
            id = int(request.GET.get('id'))
            if id > 2:
                return HttpResponse("Sorry ! request for less days")
            else:
                obj = request.POST

                emp_id = obj['emp_id']
                emp_dep = obj['emp_dep']

                obj1 = Leave.objects.filter(department__dept_name= emp_dep,lv_date = datetime.now().date()).values()
                ll=len(obj1)
                for i in range(0,ll):
                    k=obj1[i]['hod_status']
                    if k == True:
                        ll=ll-1
                        break


                obj2 = Department.objects.get(dept_name = emp_dep)
                obj3 = Employee.objects.get(empid = emp_id)
                if obj2.hod == obj3.name:                           # if the employee is head of department
                    o = Department.objects.get(dept_name = emp_dep)
                    leave_details = Leave(lv_emp_id = emp_id, department = o, lv_status = True, lv_date = datetime.now().date())
                    leave_details.save()

                # if employee is not the head  of department
                if ll >= 2:
                    o = Department.objects.get(dept_name = emp_dep)
                    leave_details = Leave(lv_emp_id = emp_id, department = o, lv_status = False, lv_date = datetime.now().date())
                    leave_details.save()
                else:
                    o = Department.objects.get(dept_name = emp_dep)
                    leave_details = Leave(lv_emp_id = emp_id, department = o, lv_status = True, lv_date = datetime.now().date())
                    leave_details.save()

            return HttpResponse('Updated', status=200)

        except ObjectDoesNotExist:
            return HttpResponse('Entry does not exist in database', status=500)

        except MultiValueDictKeyError:
            return HttpResponse('Multiple values returned in the dictionary', status=500)



# Deleting pending request
    def delete(self,request):
        try:
            id = request.GET['id']
            obj = Leave.objects.get(pk = id)
            obj.delete()

            return HttpResponse('Deleted your pending request')
        except ObjectDoesNotExist:
            return HttpResponse('Entry does not exist in database', status=500)
