from django.conf.urls import include, url
from django.contrib import admin
from EmpSys.views import EmployeeData, LeaveDetails, Leaves, LeaveInfo
from django.views.decorators.csrf import csrf_exempt
from EmpSys import views
urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^leave_current_date/',LeaveInfo.as_view(),name='get'),
    url(r'^month_pending_leave/$',csrf_exempt(Leaves.as_view()),name="EMS-get"),
    url(r'^leave_req/',EmployeeData.as_view()),
    url(r'^hod_pending/$', csrf_exempt(LeaveDetails.as_view()),name="EMS-Details"),
    url(r'^delete_req/$',csrf_exempt(EmployeeData.as_view()),name="EMS-delete"),
]
