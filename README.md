# Project Title

Leave Management System

## Description

This project basically implement some of the basic GET API's like querying number of employees on leave today, adding leave request etc.


### Prerequisites

1. Python any version must be installed in system.

2. Any type of Database like MySQl , PostgreSQL must be installed.


### Installing

Step 1. First create virtual environment using the command 'virtualenv'

Step 2. Activate the virtual environment using the command 'source bin/activate'

Step 3. Now install django version 1.8 using the command 'pip install django==1.8'


## Running the Project

Step 1. Start the Project
        'django-admin.py startproject EMS'

Step 2. Run the server
        'python manage.py runserver'

Step 3. If server is running correctly the start an app
        'python manage.py startapp EmpSys'    


## Some Urls to hit the API

admin/

^leave_current_date/ [name='get'] example(http://127.0.0.1:8000/leave_current_date/)

% When we hit this url it will give no. of employees on leave today %

^month_pending_leave/$ [name='EMS-get'] example(http://127.0.0.1:8000/month_pending_leave/?id=july)

% When we hit this url it will give no. of employees on leave, for every day of month

^leave_req/ example(http://127.0.0.1:8000/leave_req/?id=4)

% This is a POST request when we hit from the Postman it will add request of user for leave on the basis of number of days for which he/she applying %

^hod_pending/$ [name='EMS-Details'] example(http://127.0.0.1:8000/hod_pending/?id=Sanjeev)

% When we hit this url from browser it will give the number of pending requests which needs the approval of HOD %

^delete_req/$ [name='EMS-delete'] example(http://127.0.0.1:8000/delete_req/?id=11)  

% When we hit this url from Postman by passing the employee id it will delete the pending rquest of that employee from the leave table %            

## To initially populate the database with same fake entries  

Step 1. First flush the database using command 'python manage.py flush'

Step 2. Then create superuser 'python manage.py createsuperuser'

Step 3. Then open the dbshell 'python manage.py shell'

Step 4. Now just import the file 'import populate'
